
import { http } from './config'

export default{
    listar:() => {
        return http.get('eventos')
    },
    salvar:(evento) => {
        return http.post('eventos', evento)
    },
    atualizar:(evento) => {
        return http.put(`eventos/${evento.id}`, evento)
    },
    apagar:(evento) => {
        console.log(evento)
        return http.delete(`eventos/${evento.id}`)
    },
}